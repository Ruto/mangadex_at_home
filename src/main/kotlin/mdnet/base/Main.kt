/*
Mangadex@Home
Copyright (c) 2020, MangaDex Network
This file is part of MangaDex@Home.

MangaDex@Home is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MangaDex@Home is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this MangaDex@Home.  If not, see <http://www.gnu.org/licenses/>.
 */
package mdnet.base

import ch.qos.logback.classic.LoggerContext
import java.io.File
import kotlin.system.exitProcess
import mdnet.BuildInfo
import org.slf4j.LoggerFactory
import picocli.CommandLine

object Main {
    private val LOGGER = LoggerFactory.getLogger(Main::class.java)

    @JvmStatic
    fun main(args: Array<String>) {
        CommandLine(ClientArgs()).execute(*args)
    }

    fun dieWithError(e: Throwable): Nothing {
            LOGGER.error(e) { "Critical Error" }
        (LoggerFactory.getILoggerFactory() as LoggerContext).stop()
        exitProcess(1)
    }

    fun dieWithError(error: String): Nothing {
        LOGGER.error { "Critical Error: $error" }

        (LoggerFactory.getILoggerFactory() as LoggerContext).stop()
        exitProcess(1)
    }
}

@CommandLine.Command(name = "java -jar <jar>", usageHelpWidth = 120, version = ["Client Version ${BuildInfo.VERSION} (Build ${Constants.CLIENT_BUILD})"])
data class ClientArgs(
    @field:CommandLine.Option(names = ["-s", "--settings"], defaultValue = "settings.json", paramLabel = "<settings>", description = ["the settings file (default: \${DEFAULT-VALUE})"])
    var settingsFile: File = File("settings.json"),
    @field:CommandLine.Option(names = ["-d", "--database"], defaultValue = "cache\${sys:file.separator}data.db", paramLabel = "<settings>", description = ["the database file (default: \${DEFAULT-VALUE})"])
    var databaseFile: File = File("cache${File.separator}data.db"),
    @field:CommandLine.Option(names = ["-c", "--cache"], defaultValue = "cache", paramLabel = "<settings>", description = ["the cache folder (default: \${DEFAULT-VALUE})"])
    var cacheFolder: File = File("cache"),
    @field:CommandLine.Option(names = ["-h", "--help"], usageHelp = true, description = ["show this help message and exit"])
    var helpRequested: Boolean = false,
    @field:CommandLine.Option(names = ["-v", "--version"], versionHelp = true, description = ["show the version message and exit"])
    var versionRequested: Boolean = false
) : Runnable {
    override fun run() {
        println(
            "Mangadex@Home Client Version ${BuildInfo.VERSION} (Build ${Constants.CLIENT_BUILD}) initializing"
        )
        println()
        println("Copyright (c) 2020, MangaDex Network")
        println("""
            Mangadex@Home is free software: you can redistribute it and/or modify
            it under the terms of the GNU General Public License as published by
            the Free Software Foundation, either version 3 of the License, or
            (at your option) any later version.

            Mangadex@Home is distributed in the hope that it will be useful,
            but WITHOUT ANY WARRANTY; without even the implied warranty of
            MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
            GNU General Public License for more details.

            You should have received a copy of the GNU General Public License
            along with Mangadex@Home.  If not, see <https://www.gnu.org/licenses/>.
        """.trimIndent())

        val client = MangaDexClient(settingsFile, databaseFile, cacheFolder)
        Runtime.getRuntime().addShutdownHook(Thread {
            client.shutdown()
            (LoggerFactory.getILoggerFactory() as LoggerContext).stop()
        })
        client.runLoop()
    }
}
