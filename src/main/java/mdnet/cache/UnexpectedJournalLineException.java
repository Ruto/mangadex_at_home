package mdnet.cache;

import java.io.IOException;

public class UnexpectedJournalLineException extends IOException {
	public UnexpectedJournalLineException(String unexpectedLine) {
		super("unexpected journal line: " + unexpectedLine);
	}
}
